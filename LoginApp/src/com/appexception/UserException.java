package com.appexception;

public class UserException extends Exception {

	private String message;

	public UserException(String message) {
		super();  // immediate parent object 
		this.message = message;
	}

	@Override
	public String getMessage() {
		
		return message;
	} 
	
	
}
