package com.main;

import com.appexception.UserException;
import com.dao.UserDaoImpl;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class LoginAppl {

	public static void main(String[] args) {
		// scanner

		UserService userService = new UserServiceImpl();
		try {
			User obj = userService.readByUserId(333);
			// handle null pointer exception 
			if(obj != null)
			{
				System.out.println(obj.getUserId());
				System.out.println(obj.getUserName());
				System.out.println(obj.getPassword());
			}
			
		} catch (UserException e) {
			System.err.println(e.getMessage());
		}

	}

}
