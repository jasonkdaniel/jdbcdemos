package com.service;

import com.appexception.UserException;
import com.model.User;

public interface UserService {

	boolean createUser(int userid, String userName, String password) throws UserException;

	User createUser(User user); // OOP

	User readByUserId(int id) throws UserException ;// exception 

	boolean updateUser(User user);

	boolean deleteByUserID(int id);
}
