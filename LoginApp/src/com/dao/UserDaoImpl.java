package com.dao;

import com.appexception.UserException;
import com.model.User;

public class UserDaoImpl implements UserDao {

	@Override
	public boolean createUser(int userid, String userName, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public User createUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User readByUserId(int id) throws UserException {
		User user = null;
		if (id == 123456)			// hard coded  == fetched from data base 
		{
			user = new User();
			user.setUserId(id);
			user.setUserName("Happy");
			user.setPassword("learning");

		}else if (id == 223344)
		{
			user = new User();
			user.setUserId(id);
			user.setUserName("Thankyou");
			user.setPassword("Goodnight");

		}
		else {
			throw new UserException("No such user in DB");
		}
		return user;
	}

	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteByUserID(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
