package com.dao;

import com.appexception.UserException;
import com.model.User;

// JDBC  Java  database  connect
public interface UserDao {

	boolean createUser(int userid, String userName, String password);

	User createUser(User user); // OOP

	User readByUserId(int id) throws UserException ;// exception 

	boolean updateUser(User user);

	boolean deleteByUserID(int id);
}
